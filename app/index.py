import time

def count():
        lis = ["|>-----------|  0%|","|=>----------| 10%|","|==>---------| 20%|","|===>--------| 30%|","|====>-------| 40%|","|=====>------| 50%|","|======>-----| 60%|","|=======>----| 70%|","|========>---| 80%|","|=========>--| 90%|","|==========>-|100%|"]
        for i in lis:
            print("                                ",i, end="\r")
            time.sleep(0.05)

grupo = ['Gustavo Rodrigues Lima', 'Gustavo Rossini Ozzetti', 'Matheus Cardoso Morais', 'Leonardo Bispo Andreata', 'Kauã Barbosa do Nascimento']

print('Olá! Fazemos parte do 3B de Análise e Desenvolvimento de Sistemas da Impacta! =)')
time.sleep(1)

print('Grupo composto por:')
time.sleep(1)

for integrante in grupo:
    print(integrante)
    time.sleep(1)

print('')
print('Calma, só um instante, estamos calculando nossa nota...')
time.sleep(1)
count()
print('Isso mesmo, tiramos 10!')
time.sleep(1)
print('Brincadeira professor! Piadinha apenas')