**Olá professor, execute os seguintes comandos para rodar o código**

- Para construir a imagem
```bash
docker build -t devopsac2 .
```

- Para roda-la em seu terminal
```bash
docker run -it devopsac2
```

#### Integrantes do Grupo
- Gustavo Rodrigues Lima RA: 2201436
- Leonardo Bispo Andreata RA: 2201582
- Matheus Cardoso Morais RA: 2200764
- Gustavo Rossini Ozzetti RA: 2101054
- Kauã Barbosa do Nascimento RA: 2201260